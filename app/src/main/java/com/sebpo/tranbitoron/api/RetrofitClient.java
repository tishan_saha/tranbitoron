package com.sebpo.tranbitoron.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

// Using SingleTone pattern
public class RetrofitClient {

    private static final String BASE_URL = "https://dev.sebpo.net/rds/public/";
    private static RetrofitClient mInstance;
    private Retrofit retrofit;

    private RetrofitClient() {
        this.retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static synchronized RetrofitClient getInstance() {
        if(mInstance == null) {
            mInstance = new RetrofitClient();
        }
        return  mInstance;
    }

    public ApiInterface getApiInterface() {
        return retrofit.create(ApiInterface.class);
    }

}
