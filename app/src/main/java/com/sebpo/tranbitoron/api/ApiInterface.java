package com.sebpo.tranbitoron.api;

import com.sebpo.tranbitoron.model.CityCorporation;
import com.sebpo.tranbitoron.model.District;
import com.sebpo.tranbitoron.model.Division;
import com.sebpo.tranbitoron.model.Login;
import com.sebpo.tranbitoron.model.Municipality;
import com.sebpo.tranbitoron.model.Registration;
import com.sebpo.tranbitoron.model.Thana;
import com.sebpo.tranbitoron.model.Union;
import com.sebpo.tranbitoron.model.VerifyOTP;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiInterface {

    //String app-key= "$2y$10$K8TaMQPgqntU3zzz5YIor.AEg7YxlZqEHy8OEvWEONbw4ePVyM6CK";

    @FormUrlEncoded
    @Headers("app-key: $2y$10$K8TaMQPgqntU3zzz5YIor.AEg7YxlZqEHy8OEvWEONbw4ePVyM6CK")
    @POST("api/v1/user/registration")
    Call<Registration> createUser(
            @Field("fullname") String fullname,
            @Field("nid") String nid,
            @Field("mobile") String mobileNum,
            @Field("division_id") int division,
            @Field("district_id") int district,
            @Field("city_corporation_id") int cityCorporation,
            @Field("municipal_id") int municipality,
            @Field("thana_id") int thana,
            @Field("union_id") int union,
            @Field("ward") String word_village,
            @Field("area") String house_area
    );

    @FormUrlEncoded
    @Headers("app-key: $2y$10$K8TaMQPgqntU3zzz5YIor.AEg7YxlZqEHy8OEvWEONbw4ePVyM6CK")
    @POST("api/v1/user/login")
    Call<Login> login(
            @Field("nid") String nid,
            @Field("password") String password
    );

    @Headers("app-key: $2y$10$K8TaMQPgqntU3zzz5YIor.AEg7YxlZqEHy8OEvWEONbw4ePVyM6CK")
    @GET("api/v1/division/all")
    Call<Division> getDivision();

    @Headers("app-key: $2y$10$K8TaMQPgqntU3zzz5YIor.AEg7YxlZqEHy8OEvWEONbw4ePVyM6CK")
    //@GET("api/v1/district/all/6")
    @GET("api/v1/district/all/{division_id}")
    Call<District> getDistrict(@Path(value = "division_id", encoded = true) int divisionId);

    @Headers("app-key: $2y$10$K8TaMQPgqntU3zzz5YIor.AEg7YxlZqEHy8OEvWEONbw4ePVyM6CK")
    //@GET("api/v1/city-corporation/all/46")
    @GET("api/v1/city-corporation/all/{district_id}")
    Call<CityCorporation> getCityCorporation(@Path(value = "district_id", encoded = true) int districtId);

    @Headers("app-key: $2y$10$K8TaMQPgqntU3zzz5YIor.AEg7YxlZqEHy8OEvWEONbw4ePVyM6CK")
    @GET("api/v1/municipality/all/{district_id}")
    Call<Municipality> getMunicipality(@Path(value = "district_id", encoded = true) int districtId);

    @Headers("app-key: $2y$10$K8TaMQPgqntU3zzz5YIor.AEg7YxlZqEHy8OEvWEONbw4ePVyM6CK")
    @GET("api/v1/thana/all/{district_id}")
    Call<Thana> getThana(@Path(value = "district_id", encoded = true) int districtId);

    @Headers("app-key: $2y$10$K8TaMQPgqntU3zzz5YIor.AEg7YxlZqEHy8OEvWEONbw4ePVyM6CK")
    @GET("api/v1/union/all/{thana_id}")
    Call<Union> getUnion(@Path(value = "thana_id", encoded = true) int thanaId);

    @FormUrlEncoded
    @Headers("app-key: $2y$10$K8TaMQPgqntU3zzz5YIor.AEg7YxlZqEHy8OEvWEONbw4ePVyM6CK")
    @POST("api/v1/user/registration/verify/otp")
    Call<VerifyOTP> verifyOTP(
            @Field("nid") String nid,
            @Field("otp") int otp
    );

}
