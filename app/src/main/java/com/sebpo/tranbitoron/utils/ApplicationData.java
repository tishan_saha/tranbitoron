package com.sebpo.tranbitoron.utils;

import android.view.MotionEvent;
import android.view.View;
import android.widget.AutoCompleteTextView;

public class ApplicationData {

    //public static String[] divisionList = {"Barisal", "Dhaka", "Chittagong", "Khulna", "Mymensingh", "Rajshahi", "Rangpur", "Sylhet"};
    public static int otp;
    public  static String userNID;

    public static void showSuggestedData(AutoCompleteTextView touchedItem) {
        AutoCompleteTextView touched = touchedItem;
        touched.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                touched.showDropDown();
                return false;
            }
        });
    }

}
