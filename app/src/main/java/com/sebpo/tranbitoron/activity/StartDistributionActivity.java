package com.sebpo.tranbitoron.activity;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import es.dmoral.toasty.Toasty;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sebpo.tranbitoron.R;
import com.sebpo.tranbitoron.utils.DynamicViews;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class StartDistributionActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks {

    private static final int PICK_IMAGE_CAMERA = 1;
    private static final int PICK_IMAGE_GALLERY = 2;
    TextView mTitle;
    ListView listView, photoListView;
    ArrayList<String> itemList, photoPathList;
    ArrayList<Uri> photoUriList;
    EditText name, nid, photo;
    Button addBtn, photoUploadBtn, finishBtn;
    int count = 1;
    private Bitmap bitmap;
    private File destination = null;

    LinearLayout linearLayout;
    DynamicViews dynamicViews;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_distribution);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mTitle = findViewById(R.id.dist_title);
        //listView = findViewById(R.id.distListviewId);
        name = findViewById(R.id.receiverNameId);
        nid = findViewById(R.id.receiverNIDId);
        //photo = findViewById(R.id.photoId);
        photoListView = findViewById(R.id.photoListviewId);
        itemList = new ArrayList<>();
        photoUriList = new ArrayList<>();
        photoPathList = new ArrayList<>();
        addBtn = findViewById(R.id.addItemBtnId);
        photoUploadBtn = findViewById(R.id.uploadPhotoBtnId);
        finishBtn = findViewById(R.id.dist_finish_id);

        linearLayout = findViewById(R.id.dynamicViewId);

        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            mTitle.setText("Start Distribution");
        }

        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1, itemList);

        addBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void onClick(View v) {
                /*itemList.add(count++ + " | "+name.getText().toString() + " | " +nid.getText().toString());
                name.setText("");
                nid.setText("");
                adapter.notifyDataSetChanged();*/

                if(!name.getText().toString().isEmpty() && !nid.getText().toString().isEmpty()) {
                    LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View view = inflater.inflate(R.layout.dynamic_layout, null);

                    // insert value into child view of dynamic layout
                    TextView serialtv = (TextView) view.findViewById(R.id.itemSerialId);
                    serialtv.setText(""+count++);

                    TextView nametv = (TextView) view.findViewById(R.id.itemNameId);
                    nametv.setText(name.getText().toString());
                    //nametv.setTextColor(Color.BLACK);

                    TextView nidtv = (TextView) view.findViewById(R.id.itemNIDId);
                    nidtv.setText(nid.getText().toString());

                    // insert into main view
                    linearLayout.addView(view, 0, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT));

                    //name.setText("");
                    //nid.setText("");
                }

            }
        });
        //listView.setAdapter(adapter);

        photoUploadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                askForPermission();
            }
        });

        finishBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //api will call with multiple img
                //Toast.makeText(StartDistributionActivity.this, "List Submitted Successfully!!", Toast.LENGTH_SHORT).show();
                Toasty.success(StartDistributionActivity.this, "List Submitted Successfully!!", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @AfterPermissionGranted(1)
    private void askForPermission() {
        String[] perms = {Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE};
        if(EasyPermissions.hasPermissions(this, perms)){
            //Toast.makeText(StartDistributionActivity.this,"Gallery access permission already granted.", Toast.LENGTH_LONG).show();
            selectImage();
        } else {
            EasyPermissions.requestPermissions(this, "please allow to access photo", 1, perms);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        //Toast.makeText(StartDistributionActivity.this,"Permission granted.", Toast.LENGTH_LONG).show();
        Toasty.success(StartDistributionActivity.this,"Permission granted.", Toast.LENGTH_LONG).show();
        selectImage();
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        if(EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId()==android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void selectImage() {

        final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Option");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(options[which].equals("Take Photo")) {
                    dialog.dismiss();
                    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, PICK_IMAGE_CAMERA);
                } else if(options[which].equals("Choose From Gallery")) {
                    dialog.dismiss();
                    Intent pickPhotoIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhotoIntent, PICK_IMAGE_GALLERY);
                } else if(options[which].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        super.onActivityResult(requestCode, resultCode, resultData);
        if (resultCode == RESULT_OK) {
            if (requestCode == PICK_IMAGE_CAMERA) {
                try {
                    Uri selectedImageUri = resultData.getData();
                    bitmap = (Bitmap) resultData.getExtras().get("data");
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);

                    Log.e("Activity", "Pick from Camera::>>> ");

                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
                    destination = new File(Environment.getExternalStorageDirectory() + "/" +
                            getString(R.string.app_name), "IMG_" + timeStamp + ".jpg");
                    FileOutputStream fo;
                    try {
                        destination.createNewFile();
                        fo = new FileOutputStream(destination);
                        fo.write(bytes.toByteArray());
                        fo.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    /*imgPath = destination.getAbsolutePath();
                    imageview.setImageBitmap(bitmap);*/

                    photoUriList.add(selectedImageUri);     // uri list hv 2 pass on api

                    // for showing all selected img path on listview
                    photoPathList.add(destination.getAbsolutePath());
                    ArrayAdapter<String> photoAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1, photoPathList);
                    photoListView.setAdapter(photoAdapter);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == PICK_IMAGE_GALLERY) {
                Uri selectedImageUri = resultData.getData();
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImageUri);
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
                    Log.e("Activity", "Pick from Gallery::>>> ");

                    /*imgPath = getRealPathFromURI(selectedImageUri);
                    destination = new File(imgPath.toString());
                    imageview.setImageBitmap(bitmap);*/

                    photoUriList.add(selectedImageUri);     // uri list hv 2 pass on api

                    // for showing all selected img path on listview
                    photoPathList.add(getImagePath(getApplicationContext(), selectedImageUri));
                    ArrayAdapter<String> photoAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1, photoPathList);
                    photoListView.setAdapter(photoAdapter);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static String getImagePath(Context context, Uri uri ) {
        String path = null;
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = context.getContentResolver( ).query( uri, proj, null, null, null );
        if(cursor != null){
            if ( cursor.moveToFirst( ) ) {
                int column_index = cursor.getColumnIndexOrThrow( proj[0] );
                path = cursor.getString( column_index );
            }
            cursor.close( );
        }
        if(path == null) {
            path = "Not found";
        }
        return path;
    }


}
