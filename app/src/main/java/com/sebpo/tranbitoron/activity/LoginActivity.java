package com.sebpo.tranbitoron.activity;

import androidx.appcompat.app.AppCompatActivity;
import es.dmoral.toasty.Toasty;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.sebpo.tranbitoron.R;
import com.sebpo.tranbitoron.api.RetrofitClient;
import com.sebpo.tranbitoron.model.Login;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;

public class LoginActivity extends AppCompatActivity {


    public TextView forgotPassTxt, registerTxt;
    public Button loginBtn;
    public EditText nidFld, passwordFld;
    ProgressBar progressBar;

    String nidValue, password;
    private String status = null, nidError, passwordError;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        nidFld =  findViewById(R.id.nid_field_id);
        passwordFld = findViewById(R.id.password_field_id);
        loginBtn = findViewById(R.id.login_btn);
        forgotPassTxt = findViewById(R.id.forgetpassword_textView_id);
        progressBar = findViewById(R.id.progressBarId);

        //text underline code
        forgotPassTxt.setPaintFlags(forgotPassTxt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        registerTxt = findViewById(R.id.register_textView_id);
        registerTxt.setPaintFlags(registerTxt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);


        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                nidValue = nidFld.getText().toString();
                password = passwordFld.getText().toString();

                if(nidValue.isEmpty()) {
                    nidFld.setError("NID is required");
                    return;
                }
                if(password.isEmpty()) {
                    passwordFld.setError("Password is required");
                    return;
                }

                loginApiCall();

            }
        });

        registerTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, RegistrationActivity.class);
                startActivity(i);
            }
        });

        forgotPassTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getApplicationContext(),"Password has been sent to your mobile", Toast.LENGTH_SHORT).show();
                Toasty.info(getApplicationContext(),"Password has been sent to your mobile", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void loginApiCall() {
        progressBar.setVisibility(View.VISIBLE);
        Call<Login> call = RetrofitClient
                .getInstance()
                .getApiInterface()
                //.login(3740502612L,"Rd$mnk24");
                .login(nidValue,password);

        call.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                progressBar.setVisibility(View.GONE);
                Login login = null;
                try {
                    login = response.body();

                    //status = login.getStatus();
                    /*if ((status.equals("success"))) {
                        Toast.makeText(getApplicationContext(), login.getMessage(), Toast.LENGTH_LONG).show();
                    } else if ((status.equals("failed"))) {
                        Toast.makeText(getApplicationContext(), login.getMessage(), Toast.LENGTH_LONG).show();
                    } else if ((status.equals("error"))) {
                        Toast.makeText(getApplicationContext(), "NID must be 10, 13 OR 17 digits long and should be numeric.", Toast.LENGTH_LONG).show();
                    }*/

                    //have to check response code and handle error json body
                    if ((response.code() == 200)) {
                        status = login.getStatus();     // it's work here bt not outside
                        //Toast.makeText(getApplicationContext(), login.getMessage(), Toast.LENGTH_LONG).show();
                        if ((status.equals("success"))) {
                            Toasty.success(getApplicationContext(), login.getMessage(), Toast.LENGTH_SHORT).show();
                            Intent i=new Intent(LoginActivity.this, HomeActivity.class);
                            startActivity(i);
                        } else if (status.equals("failed")) {
                            Toasty.warning(getApplicationContext(), login.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        try {
                            //JSONObject jObjError = new JSONObject(response.errorBody().string());
                            //Toast.makeText(LoginActivity.this, jObjError.getString("message"), Toast.LENGTH_LONG).show();
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            String messageError = jObjError.getString("message");

                            nidError = "";
                            passwordError = "";
                            JSONObject jObjMsgError = new JSONObject(messageError);
                            nidError = jObjMsgError.getString("nid");

                            if(messageError.contains("password")) {
                                passwordError = jObjMsgError.getString("password");
                                //Toast.makeText(LoginActivity.this, nidError+"\n"+passwordError, Toast.LENGTH_LONG).show();
                                Toasty.error(getApplicationContext(), nidError+"\n"+passwordError, Toast.LENGTH_LONG).show();
                            } else {
                                //Toast.makeText(LoginActivity.this, nidError, Toast.LENGTH_LONG).show();
                                Toasty.error(getApplicationContext(), nidError, Toast.LENGTH_LONG).show();
                            }

                        } catch (Exception e) {
                            //Toast.makeText(LoginActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                            Toasty.error(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }

                } catch (NullPointerException e) {
                    //Toast.makeText(LoginActivity.this,"Server error",Toast.LENGTH_SHORT).show();
                    Toasty.error(getApplicationContext(),"Server error",Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                //Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                Toasty.error(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });


    }

}