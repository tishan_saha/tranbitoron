package com.sebpo.tranbitoron.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import es.dmoral.toasty.Toasty;

import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.sebpo.tranbitoron.R;

public class ReliefRequestForMeActivity extends AppCompatActivity {

    TextView mTitle;
    Button submit;
    boolean submitResponse = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relief_request_for_me);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mTitle = findViewById(R.id.relief_request_me_title);
        submit = findViewById(R.id.relief_request_for_me_submit_id);

        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            mTitle.setText("Relief Request for Me");
        }

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(submitResponse==true) {  //check later
                    //Toast.makeText(getApplicationContext(), "Request Success!!", Toast.LENGTH_LONG).show();
                    Toasty.success(getApplicationContext(), "Request Success!!", Toast.LENGTH_LONG).show();
                    submit.setEnabled(false);
                    //submit.setVisibility(View.INVISIBLE);
                    submit.setBackgroundColor(Color.GRAY);
                } else {
                    //Toast.makeText(getApplicationContext(), "Please Try Again", Toast.LENGTH_LONG).show();
                    Toasty.error(getApplicationContext(), "Please Try Again", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId()==android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
