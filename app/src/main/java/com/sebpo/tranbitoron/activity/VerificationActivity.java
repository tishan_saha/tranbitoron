package com.sebpo.tranbitoron.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.goodiebag.pinview.Pinview;
import com.sebpo.tranbitoron.R;
import com.sebpo.tranbitoron.api.RetrofitClient;
import com.sebpo.tranbitoron.model.VerifyOTP;
import com.sebpo.tranbitoron.utils.ApplicationData;

import org.json.JSONObject;

public class VerificationActivity extends AppCompatActivity {

    TextView resendCodeTxt, requestTxt, successTxt, mTitle ;
    Button verCodeBtn, verNextBtn;
    String pinCodeValue;
    int pinNumber;
    ProgressBar progressBar;
    Pinview pinview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mTitle = findViewById(R.id.reg_ver_title);

        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            mTitle.setText("User Registration");
        }

        requestTxt = findViewById(R.id.request_textView_id);
        successTxt = findViewById(R.id.success_textView_id);
        verCodeBtn = findViewById(R.id.submit_btn_verification_id);
        verNextBtn = findViewById(R.id.next_btn_verification_id);
        progressBar = findViewById(R.id.progressBarId);
        pinview = findViewById(R.id.pinviewId);

        resendCodeTxt = findViewById(R.id.resend_code_textview_id);
        resendCodeTxt.setPaintFlags(resendCodeTxt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        pinview.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean fromUser) {
                //Toast.makeText(VerificationActivity.this, pinview.getValue(), Toast.LENGTH_SHORT).show();
                pinCodeValue = pinview.getValue();
            }
        });

        verCodeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("===="+pinCodeValue);
                pinNumber = Integer.valueOf(pinCodeValue);
                System.out.println("===="+ApplicationData.userNID);
                System.out.println("===="+pinNumber);

                verifyOTPApiCall();

            }
        });

        verNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(VerificationActivity.this, HomeActivity.class);
                startActivity(intent);

            }
        });

    }

    private void verifyOTPApiCall() {
        progressBar.setVisibility(View.VISIBLE);
        Call<VerifyOTP> verifyOTPCall = RetrofitClient
                .getInstance()
                .getApiInterface()
                .verifyOTP(ApplicationData.userNID, pinNumber);

        verifyOTPCall.enqueue(new Callback<VerifyOTP>() {
            @Override
            public void onResponse(Call<VerifyOTP> call, Response<VerifyOTP> response) {
                progressBar.setVisibility(View.GONE);
                VerifyOTP verifyOTP = null;

                try {
                    verifyOTP = response.body();

                    if((response.code() == 200)) {
                        verCodeBtn.setVisibility(View.INVISIBLE);
                        pinview.setVisibility(View.INVISIBLE);
                        requestTxt.setVisibility(View.INVISIBLE);
                        //successTxt.setText("Registration Success");
                        successTxt.setText(verifyOTP.getMessage());
                        verNextBtn.setVisibility(View.VISIBLE);
                    } else {
                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            //Toast.makeText(VerificationActivity.this, jObjError.getString("message"), Toast.LENGTH_LONG).show();
                            Toasty.error(VerificationActivity.this, jObjError.getString("message"), Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                            //Toast.makeText(VerificationActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                            Toasty.error(VerificationActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }

                } catch (Exception e) {
                    //e.printStackTrace();
                    //Toast.makeText(VerificationActivity.this,"Server error",Toast.LENGTH_SHORT).show();
                    Toasty.error(VerificationActivity.this,"Server error",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<VerifyOTP> call, Throwable t) {
                //Toast.makeText(VerificationActivity.this,"Error occured "+t.toString(),Toast.LENGTH_SHORT).show();
                Toasty.error(VerificationActivity.this,"Error occured "+t.toString(),Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId()==android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
