package com.sebpo.tranbitoron.activity;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;

import com.sebpo.tranbitoron.R;

public class ReliefDistributionStatusActivity extends AppCompatActivity {

    TextView mTitle;
    String relief_id[] = {"130", "123", "126", "135", "139", "140", "150", "160", "170", "180", "190", "200", "155", "144", "133"};
    String names[] = {"SEBPO CSR", "Nafiur Rahman", "Mahmud Hossain", "My NGO",
            "NGO2", "NGO3", "NGO4", "NGO5", "NGO6",
            "NGO7", "X", "Y", "Z", "NGO8", "NGO9"};
    String dates[] = {"30/5/2020", "10/6/2020", "31/5/2020", "28/5/2020", "25/5/2020",
            "3/6/2020", "4/6/2020", "5/6/2020", "6/6/2020", "7/6/2020",
            "8/6/2020", "9/6/2020", "10/6/2020", "11/6/2020", "12/6/2020"};

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relief_distribution_status);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mTitle = findViewById(R.id.rds_title);

        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            mTitle.setText("Relief Distribution Status");
        }

        addHeaders();
        addData();

    }

    /**
     * This function add the headers to the table
     **/
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void addHeaders() {
        //TableLayout tl = findViewById(R.id.table);
        TableLayout tl = findViewById(R.id.tableHeader);
        TableRow tr = new TableRow(this);
        tr.setLayoutParams(getLayoutParams());
        tr.addView(getTextView(0, "Id"));
        tr.addView(getTextView(0, "By"));
        tr.addView(getTextView(0, "Date"));
        tr.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.border));
        tl.addView(tr, getTblLayoutParams());
    }

    /**
     * This function add the data to the table
     * Here api will call and get the response data
     **/
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void addData() {
        int numofRows = names.length;
        TableLayout tl = findViewById(R.id.table);
        for (int i = 0; i < numofRows; i++) {
            TableRow tr = new TableRow(this);
            tr.setLayoutParams(getLayoutParams());
            tr.addView(getTextView(i + 1, relief_id[i]));
            tr.addView(getTextView(i, names[i]));
            tr.addView(getTextView(i + numofRows, dates[i]));
            tr.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.border));
            tl.addView(tr, getTblLayoutParams());
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    private TextView getTextView(int id, String title) {
        TextView tv = new TextView(this);
        tv.setId(id);
        tv.setText(title);
        tv.setTextColor(Color.BLACK);
        tv.setPadding(20,20,20,20);
        //tv.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.border));
        tv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        tv.setLayoutParams(getLayoutParams());
        return tv;
    }

    @NonNull
    private LayoutParams getLayoutParams() {
        LayoutParams params = new LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 0, 0, 2);
        return params;
    }

    @NonNull
    private TableLayout.LayoutParams getTblLayoutParams() {
        return new TableLayout.LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId()==android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
