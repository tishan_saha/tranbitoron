package com.sebpo.tranbitoron.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import es.dmoral.toasty.Toasty;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.sebpo.tranbitoron.R;

public class ReliefRequestForOthersSubmissionActivity extends AppCompatActivity {

    TextView mTitle, backMainMenu;
    Button submit;
    boolean submitResponse = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relief_request_for_others_submission);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mTitle = findViewById(R.id.relief_request_others_title);
        submit = findViewById(R.id.relief_request_for_others_confirm_submit_id);
        backMainMenu = findViewById(R.id.backMainMenuId);

        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            mTitle.setText("Relief Request for Others");
        }

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(submitResponse==true) {  //check later
                    //Toast.makeText(getApplicationContext(), "Request Success!!", Toast.LENGTH_LONG).show();
                    Toasty.success(getApplicationContext(), "Request Success!!", Toast.LENGTH_LONG).show();
                    submit.setEnabled(false);
                    //submit.setVisibility(View.INVISIBLE);
                    submit.setBackgroundColor(Color.GRAY);
                } else {
                    //Toast.makeText(getApplicationContext(), "Please Try Again", Toast.LENGTH_LONG).show();
                    Toasty.error(getApplicationContext(), "Please Try Again", Toast.LENGTH_LONG).show();
                }
            }
        });

        backMainMenu.setPaintFlags(backMainMenu.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        backMainMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ReliefRequestForOthersSubmissionActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId()==android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
