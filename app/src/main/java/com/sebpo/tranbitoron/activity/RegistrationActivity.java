package com.sebpo.tranbitoron.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.sebpo.tranbitoron.R;
import com.sebpo.tranbitoron.api.RetrofitClient;
import com.sebpo.tranbitoron.model.CityCorporation;
import com.sebpo.tranbitoron.model.District;
import com.sebpo.tranbitoron.model.Division;
import com.sebpo.tranbitoron.model.Municipality;
import com.sebpo.tranbitoron.model.Registration;
import com.sebpo.tranbitoron.model.Thana;
import com.sebpo.tranbitoron.model.Union;
import com.sebpo.tranbitoron.utils.ApplicationData;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class RegistrationActivity extends AppCompatActivity {

    TextView mTitle;
    Button submitBtn;
    EditText fullname, nid, mobileNum, word_village, house_area;
    AutoCompleteTextView division, district, cityCorporation, municipality, thana, union;

    String fullnameValue, nidValue, mobileNumValue, word_villageValue, house_areaValue;
    public static int divisionId, districtId, cityCorporationId, municipalityId, thanaId, unionId;
    public static boolean isDivisionSelect, isDistrictSelect, isCityCorpSelect, isMuniciSelect, isThanaSelect, isUnionSelect;
    private String nidError, mobileError;

    ProgressBar progressBar;

    HashMap<Integer,String> divisionListHM = null;
    ArrayList<String> divisionNames = null;
    HashMap<Integer,String> districtListHM = null;
    ArrayList<String> districtNames = null;
    HashMap<Integer,String> cityCorporationListHM = null;
    ArrayList<String> cityCorporationNames = null;
    HashMap<Integer,String> municipalityListHM = null;
    ArrayList<String> municipalityNames = null;
    HashMap<Integer,String> thanaListHM = null;
    ArrayList<String> thanaNames = null;
    HashMap<Integer,String> unionListHM = null;
    ArrayList<String> unionNames = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mTitle = findViewById(R.id.reg_title);

        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            mTitle.setText("User Registration");
        }

        divisionListHM = new HashMap<>();
        divisionNames = new ArrayList<>();
        districtListHM = new HashMap<>();
        districtNames = new ArrayList<>();
        cityCorporationListHM = new HashMap<>();
        cityCorporationNames = new ArrayList<>();
        municipalityListHM = new HashMap<>();
        municipalityNames = new ArrayList<>();
        thanaListHM = new HashMap<>();
        thanaNames = new ArrayList<>();
        unionListHM = new HashMap<>();
        unionNames = new ArrayList<>();

        submitBtn = findViewById(R.id.submit_btn_registration_id);

        fullname = findViewById(R.id.regFullnameId);
        nid = findViewById(R.id.regnidId);
        mobileNum = findViewById(R.id.regMobileId);
        word_village = findViewById(R.id.regWordId);
        house_area = findViewById(R.id.regHouseId);

        division = findViewById(R.id.regDevisionId);
        district = findViewById(R.id.regDistrictId);
        cityCorporation = findViewById(R.id.regCitycorpId);
        municipality = findViewById(R.id.regMuniciId);
        thana = findViewById(R.id.regThanaId);
        union = findViewById(R.id.regUnionId);
        progressBar = findViewById(R.id.progressBarId);

        // call division api on load first, to show division autoCompleteText
        divisionApiCall();

        //ArrayAdapter<String> adapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1, ApplicationData.divisionList);
        ArrayAdapter<String> divisionAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1, divisionNames);
        division.setThreshold(1);
        division.setAdapter(divisionAdapter);
        // show all suggested data without typing
        //ApplicationData.showSuggestedData(division);

        division.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                isDivisionSelect = true;
                //String selectedDivision = parent.getItemAtPosition(position).toString();
                String selectedDivision = division.getText().toString();
                Log.i("selectedDivision :",selectedDivision);
                for (Map.Entry<Integer, String> map : divisionListHM.entrySet()) {
                    if(map.getValue().equals(selectedDivision)) {
                        divisionId = map.getKey();
                    }
                }
                Log.i("selectedDivisionId :",""+divisionId);
                districtApiCall();
            }
        });
        ArrayAdapter<String> districtAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1, districtNames);
        district.setThreshold(1);
        district.setAdapter(districtAdapter);
        //ApplicationData.showSuggestedData(district);

        district.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                isDistrictSelect = true;
                String selectedDistrict = district.getText().toString();
                Log.i("selectedDistrict :",selectedDistrict);
                for (Map.Entry<Integer, String> map : districtListHM.entrySet()) {
                    if(map.getValue().equals(selectedDistrict)) {
                        districtId = map.getKey();
                    }
                }
                Log.i("selectedDistrictId :",""+districtId);
                cityCorporationApiCall();
                municipalityApiCall();
                thanaApiCall();
            }
        });
        ArrayAdapter<String> cityCorporationAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1, cityCorporationNames);
        cityCorporation.setThreshold(1);
        cityCorporation.setAdapter(cityCorporationAdapter);

        ArrayAdapter<String> municipalityAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1, municipalityNames);
        municipality.setThreshold(1);
        municipality.setAdapter(municipalityAdapter);
        ApplicationData.showSuggestedData(municipality);

        municipality.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                isMuniciSelect = true;
                String selectedMunicipality = municipality.getText().toString();
                Log.i("selectedMunicipality :",selectedMunicipality);
                for (Map.Entry<Integer, String> map : municipalityListHM.entrySet()) {
                    if(map.getValue().equals(selectedMunicipality)) {
                        municipalityId = map.getKey();
                    }
                }
                Log.i("selectedMunicipalId :",""+municipalityId);
            }
        });
        ArrayAdapter<String> thanaAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1, thanaNames);
        thana.setThreshold(1);
        thana.setAdapter(thanaAdapter);
        ApplicationData.showSuggestedData(thana);

        thana.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                isThanaSelect = true;
                String selectedThana = thana.getText().toString();
                Log.i("selectedThana :",selectedThana);
                for (Map.Entry<Integer, String> map : thanaListHM.entrySet()) {
                    if(map.getValue().equals(selectedThana)) {
                        thanaId = map.getKey();
                    }
                }
                Log.i("selectedThanaId :",""+thanaId);
                unionApiCall();
            }
        });
        ArrayAdapter<String> unionAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1, unionNames);
        union.setThreshold(1);
        union.setAdapter(unionAdapter);
        ApplicationData.showSuggestedData(union);

        union.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                isUnionSelect = true;
                String selectedUnion = union.getText().toString();
                Log.i("selectedUnion :",selectedUnion);
                for (Map.Entry<Integer, String> map : unionListHM.entrySet()) {
                    if(map.getValue().equals(selectedUnion)) {
                        unionId = map.getKey();
                    }
                }
                Log.i("selectedUnionId :",""+unionId);
            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fullnameValue = fullname.getText().toString();
                nidValue = nid.getText().toString();
                mobileNumValue = mobileNum.getText().toString();
                word_villageValue = word_village.getText().toString();
                house_areaValue = house_area.getText().toString();

                if(fullnameValue.isEmpty() || nidValue.isEmpty() || mobileNumValue.isEmpty()
                        || division.getText().toString().isEmpty() || district.getText().toString().isEmpty()){
                    nid.setError("NID must be 10/13/17 digits");
                    mobileNum.setError("Mobile number should be 11 digits");
                    //Toast.makeText(getApplicationContext(),"Please fill up all required field",Toast.LENGTH_LONG).show();
                    Toasty.warning(getApplicationContext(),"Please fill up all required field",Toast.LENGTH_LONG).show();
                    //return;
                } else{

                    System.out.println("======="+fullnameValue);
                    System.out.println("======="+nidValue);
                    System.out.println("======="+mobileNumValue);
                    System.out.println("======="+divisionId);
                    System.out.println("======="+districtId);
                    System.out.println("======="+cityCorporationId);
                    System.out.println("======="+municipalityId);
                    System.out.println("======="+thanaId);
                    System.out.println("======="+unionId);
                    System.out.println("======="+word_villageValue);
                    System.out.println("======="+house_areaValue);
                    System.out.println("======="+isMuniciSelect);
                    System.out.println("======="+unionListHM.size());
                    System.out.println("======="+unionNames.size());

                    ApplicationData.userNID = nidValue;

                    boolean isValidInputSelect = isSelectedFromSuggestion();

                    if (isValidInputSelect)
                        registrationApiAll();
                    else
                        //Toast.makeText(RegistrationActivity.this, "Please select a valid input from Suggestion", Toast.LENGTH_LONG ).show();
                        Toasty.warning(RegistrationActivity.this, "Please select a valid input from Suggestion", Toast.LENGTH_LONG ).show();
                }



            }
        });


    }

    private boolean isSelectedFromSuggestion() {
        if( (!division.getText().toString().isEmpty() && isDivisionSelect==false)
                || (!district.getText().toString().isEmpty() && isDistrictSelect==false)
                || (!municipality.getText().toString().isEmpty() && isMuniciSelect==false)
                || (!thana.getText().toString().isEmpty() && isThanaSelect==false)
                || (!union.getText().toString().isEmpty() && isUnionSelect==false) ) {
            return false;
        } else
            return true;
    }

    private void registrationApiAll() {
        progressBar.setVisibility(View.VISIBLE);
        Call<Registration> registrationCall = RetrofitClient
                .getInstance()
                .getApiInterface()
                .createUser(fullnameValue, nidValue, mobileNumValue, divisionId, districtId, cityCorporationId, municipalityId, thanaId, unionId, word_villageValue, house_areaValue);

        registrationCall.enqueue(new Callback<Registration>() {
            @Override
            public void onResponse(Call<Registration> call, Response<Registration> response) {
                progressBar.setVisibility(View.GONE);
                Registration registration = null;

                try {
                    registration = response.body();

                    //For Retrofit limitation there has no perfect way to get response body except success/200 OK.
                    //if(registration.getStatus().equals("success")) {
                    if((response.code() == 200)) {
                        //OTP api will be call here after Registration success, now OTP show in toast msg
                        ApplicationData.otp = registration.getOtp();
                        //Toast.makeText(RegistrationActivity.this,"Your OTP is: "+registration.getOtp(),Toast.LENGTH_LONG).show();
                        Toasty.info(RegistrationActivity.this,"Your OTP is: "+registration.getOtp(),Toast.LENGTH_LONG).show();

                        Intent i=new Intent(RegistrationActivity.this, VerificationActivity.class);
                        startActivity(i);

                    } else {
                        try {
                            /*JSONObject jObjError = new JSONObject(response.errorBody().string());
                            Toast.makeText(RegistrationActivity.this, jObjError.getString("message"), Toast.LENGTH_LONG).show();*/
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            String messageError = jObjError.getString("message");

                            JSONObject jObjMsgError = new JSONObject(messageError);
                            nidError = "";
                            mobileError = "";
                            if(messageError.contains("nid")) {
                                nidError = jObjMsgError.getString("nid");
                            }
                            if(messageError.contains("mobile")) {
                                mobileError = jObjMsgError.getString("mobile");
                            }
                            //Toast.makeText(RegistrationActivity.this, nidError+"\n"+mobileError, Toast.LENGTH_LONG).show();
                            Toasty.error(RegistrationActivity.this, nidError+"\n"+mobileError, Toast.LENGTH_LONG).show();

                        } catch (Exception e) {
                            //Toast.makeText(RegistrationActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                            Toasty.error(RegistrationActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }

                    /*if((response.code() == 400)) {
                        Toast.makeText(RegistrationActivity.this,"NID & Mobile number should be unique and valid",Toast.LENGTH_SHORT).show();
                    }

                    if((response.code() == 500)) {
                        Toast.makeText(RegistrationActivity.this,"Server error",Toast.LENGTH_SHORT).show();
                    }*/

                } catch (Exception e) {
                    //e.printStackTrace();
                    //Toast.makeText(RegistrationActivity.this,"Server error",Toast.LENGTH_SHORT).show();
                    Toasty.error(RegistrationActivity.this,"Server error",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Registration> call, Throwable t) {
                //Toast.makeText(RegistrationActivity.this,"Error occured "+t.toString(),Toast.LENGTH_SHORT).show();
                Toasty.error(RegistrationActivity.this,"Error occured "+t.toString(),Toast.LENGTH_SHORT).show();
            }
        });

    }

    // call on thana auto suggestion item selection
    private void unionApiCall() {
        unionListHM.clear();        //to clear previous touched value
        unionNames.clear();
        Call<Union> unionCall = RetrofitClient
                .getInstance()
                .getApiInterface()
                .getUnion(thanaId);

        unionCall.enqueue(new Callback<Union>() {
            @Override
            public void onResponse(Call<Union> call, Response<Union> response) {
                Union union = null;

                try {
                    union = response.body();

                    if(union.getStatus().equals("success")) {
                        for (int i = 0; i < union.getData().size(); i++) {
                            int id = union.getData().get(i).getId();
                            String name = union.getData().get(i).getName();

                            unionListHM.put(id,name);
                            Log.i("union id", ""+id);
                            Log.i("union name", name);
                        }

                        for (Map.Entry<Integer, String> map : unionListHM.entrySet()) {
                            unionNames.add(map.getValue());
                        }

                    } else {
                        //Toast.makeText(RegistrationActivity.this,"Server error",Toast.LENGTH_SHORT).show();
                        Toasty.error(RegistrationActivity.this,"Server error",Toast.LENGTH_SHORT).show();

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Union> call, Throwable t) {

            }
        });
    }

    // call on district auto suggestion item selection
    private void thanaApiCall() {
        thanaListHM.clear();
        thanaNames.clear();
        Call<Thana> thanaCall = RetrofitClient
                .getInstance()
                .getApiInterface()
                .getThana(districtId);

        thanaCall.enqueue(new Callback<Thana>() {
            @Override
            public void onResponse(Call<Thana> call, Response<Thana> response) {
                Thana thana = null;

                try {
                    thana = response.body();

                    if(thana.getStatus().equals("success")) {
                        for (int i = 0; i < thana.getData().size(); i++) {
                            int id = thana.getData().get(i).getId();
                            String name = thana.getData().get(i).getName();

                            thanaListHM.put(id,name);
                            Log.i("thana id", ""+id);
                            Log.i("thana name", name);
                        }

                        for (Map.Entry<Integer, String> map : thanaListHM.entrySet()) {
                            thanaNames.add(map.getValue());
                        }

                    } else {
                        //Toast.makeText(RegistrationActivity.this,"Server error",Toast.LENGTH_SHORT).show();
                        Toasty.error(RegistrationActivity.this,"Server error",Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Thana> call, Throwable t) {

            }
        });
    }

    // call on district auto suggestion item selection
    private void municipalityApiCall() {
        municipalityListHM.clear();
        municipalityNames.clear();
        Call<Municipality> municipalityCall = RetrofitClient
                .getInstance()
                .getApiInterface()
                .getMunicipality(districtId);

        municipalityCall.enqueue(new Callback<Municipality>() {
            @Override
            public void onResponse(Call<Municipality> call, Response<Municipality> response) {
                Municipality municipality = null;

                try {
                    municipality = response.body();

                    if(municipality.getStatus().equals("success")) {
                        for (int i = 0; i < municipality.getData().size(); i++) {
                            int id = municipality.getData().get(i).getId();
                            String name = municipality.getData().get(i).getName();

                            municipalityListHM.put(id,name);
                            Log.i("municipality id", ""+id);
                            Log.i("municipality name", name);
                        }

                        for (Map.Entry<Integer, String> map : municipalityListHM.entrySet()) {
                            municipalityNames.add(map.getValue());
                        }

                    } else {
                        //Toast.makeText(RegistrationActivity.this,"Server error",Toast.LENGTH_SHORT).show();
                        Toasty.error(RegistrationActivity.this,"Server error",Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Municipality> call, Throwable t) {

            }
        });
    }

    // call on district auto suggestion item selection
    private void cityCorporationApiCall() {
        Call<CityCorporation> cityCorporationCall = RetrofitClient
                .getInstance()
                .getApiInterface()
                .getCityCorporation(districtId);

        cityCorporationCall.enqueue(new Callback<CityCorporation>() {
            @Override
            public void onResponse(Call<CityCorporation> call, Response<CityCorporation> response) {
                CityCorporation cityCorporation = null;

                try {
                    cityCorporation = response.body();

                    if(cityCorporation.getStatus().equals("success")) {
                        for (int i = 0; i < cityCorporation.getData().size(); i++) {
                            int id = cityCorporation.getData().get(i).getId();
                            String name = cityCorporation.getData().get(i).getName();

                            cityCorporationListHM.put(id,name);
                            Log.i("cityCorporation id", ""+id);
                            Log.i("cityCorporation name", name);
                        }

                        for (Map.Entry<Integer, String> map : cityCorporationListHM.entrySet()) {
                            cityCorporationNames.add(map.getValue());
                        }

                    } else {
                        //Toast.makeText(RegistrationActivity.this,"Server error",Toast.LENGTH_SHORT).show();
                        Toasty.error(RegistrationActivity.this,"Server error",Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<CityCorporation> call, Throwable t) {

            }
        });
    }

    // call on division auto suggestion item selection
    private void districtApiCall() {
        Call<District> districtCall = RetrofitClient
                .getInstance()
                .getApiInterface()
                .getDistrict(divisionId);

        districtCall.enqueue(new Callback<District>() {
            @Override
            public void onResponse(Call<District> call, Response<District> response) {
                District district = null;

                try {
                    district = response.body();

                    if(district.getStatus().equals("success")) {
                        for (int i = 0; i < district.getData().size(); i++) {
                            int id = district.getData().get(i).getId();
                            String name = district.getData().get(i).getName();

                            districtListHM.put(id,name);
                            Log.i("district id", ""+id);
                            Log.i("district name", name);
                        }

                        for (Map.Entry<Integer, String> map : districtListHM.entrySet()) {
                            districtNames.add(map.getValue());
                        }

                    } else {
                        //Toast.makeText(RegistrationActivity.this,"Server error",Toast.LENGTH_SHORT).show();
                        Toasty.error(RegistrationActivity.this,"Server error",Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<District> call, Throwable t) {

            }
        });

    }

    private void divisionApiCall() {

        Call<Division> divisionCall = RetrofitClient
                .getInstance()
                .getApiInterface()
                .getDivision();

        divisionCall.enqueue(new Callback<Division>() {
            @Override
            public void onResponse(Call<Division> call, Response<Division> response) {

                Division division = null;
                try {
                    division = response.body();

                    if(division.getStatus().equals("success")) {
                        for (int i = 0; i < division.getData().size(); i++) {
                            int id = division.getData().get(i).getId();
                            String name = division.getData().get(i).getName();

                            divisionListHM.put(id,name);
                            Log.i("division id", ""+id);
                            Log.i("division name", name);
                            //divisionNames.add(name);
                        }

                        for (Map.Entry<Integer, String> map : divisionListHM.entrySet()) {
                            divisionNames.add(map.getValue());
                        }

                    } else {
                        //Toast.makeText(RegistrationActivity.this,"Server error",Toast.LENGTH_SHORT).show();
                        Toasty.error(RegistrationActivity.this,"Server error",Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<Division> call, Throwable t) {
                Log.e("onFailure", t.toString());
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId()==android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
