package com.sebpo.tranbitoron.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.sebpo.tranbitoron.R;

public class ReliefRequestForOthersFormActivity extends AppCompatActivity {

    TextView mTitle;
    Button submitBtn;
    EditText fullname, nid, mobileNum, word_village, house_area;
    AutoCompleteTextView division, district, cityCorporation, municipality, thana, union;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relief_request_for_others_form);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mTitle = findViewById(R.id.relief_request_others_title);

        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            mTitle.setText("Relief Request for Others");
        }

        submitBtn = findViewById(R.id.othersFormSubmitId);

        fullname = findViewById(R.id.othersFullnameId);
        nid = findViewById(R.id.othersFormNidId);
        mobileNum = findViewById(R.id.othersMobileId);
        word_village = findViewById(R.id.othersFormWordId);
        house_area = findViewById(R.id.othersFormHouseId);

        division = findViewById(R.id.othersDevisionId);
        district = findViewById(R.id.othersDistrictId);
        cityCorporation = findViewById(R.id.othersCityCorpId);
        municipality = findViewById(R.id.othersMuniciId);
        thana = findViewById(R.id.othersThanaId);
        union = findViewById(R.id.othersUnionId);

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ReliefRequestForOthersFormActivity.this, ReliefRequestForOthersSubmissionActivity.class);
                startActivity(intent);
                //finish();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId()==android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
