package com.sebpo.tranbitoron.activity;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.sebpo.tranbitoron.R;

public class HomeActivity extends AppCompatActivity {

    TextView mTitle;
    Button reliefRequestForMeBtn, reliefRequestForOthersBtn, reliefDistributionRequestBtn, startDistributionBtn, reliefDistributionStatusBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mTitle = findViewById(R.id.main_menu_title);

        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            mTitle.setText("Main Menu");
        }

        reliefRequestForMeBtn = findViewById(R.id.relief_request_for_me_id);
        reliefRequestForOthersBtn = findViewById(R.id.relief_request_for_others_id);
        reliefDistributionRequestBtn = findViewById(R.id.relief_distribution_request_id);
        startDistributionBtn = findViewById(R.id.start_distribution_id);
        reliefDistributionStatusBtn = findViewById(R.id.relief_distribution_status_id);

        reliefRequestForMeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, ReliefRequestForMeActivity.class);
                startActivity(intent);
            }
        });

        reliefRequestForOthersBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, ReliefRequestForOthersFormActivity.class);
                startActivity(intent);
            }
        });

        reliefDistributionRequestBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, ReliefDistributionRequestActivity.class);
                startActivity(intent);
            }
        });
        startDistributionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, StartDistributionActivity.class);
                startActivity(intent);
            }
        });
        reliefDistributionStatusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, ReliefDistributionStatusActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.options_menu, menu);
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.logout:
                logout();
                //finish();
                finishAffinity();       // after back press no activity will come
                Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void logout() {
        SharedPreferences sharedPreferences = getSharedPreferences("HomeActivity", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

}
