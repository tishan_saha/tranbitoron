
package com.sebpo.tranbitoron.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VerifyOTPData {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("fullname")
    @Expose
    private String fullname;
    @SerializedName("nid")
    @Expose
    private String nid;
    @SerializedName("designation")
    @Expose
    private Object designation;
    @SerializedName("dcoffice_name")
    @Expose
    private Object dcofficeName;
    @SerializedName("division_id")
    @Expose
    private Integer divisionId;
    @SerializedName("district_id")
    @Expose
    private Integer districtId;
    @SerializedName("city_corporation_id")
    @Expose
    private Integer cityCorporationId;
    @SerializedName("municipal_id")
    @Expose
    private Integer municipalId;
    @SerializedName("thana_id")
    @Expose
    private Integer thanaId;
    @SerializedName("union_id")
    @Expose
    private Integer unionId;
    @SerializedName("ward")
    @Expose
    private Object ward;
    @SerializedName("area")
    @Expose
    private Object area;
    @SerializedName("eid")
    @Expose
    private Object eid;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("email")
    @Expose
    private Object email;
    @SerializedName("email_verified_at")
    @Expose
    private Object emailVerifiedAt;
    @SerializedName("otp")
    @Expose
    private String otp;
    @SerializedName("sms_verified")
    @Expose
    private Integer smsVerified;
    @SerializedName("api_token")
    @Expose
    private Object apiToken;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getNid() {
        return nid;
    }

    public void setNid(String nid) {
        this.nid = nid;
    }

    public Object getDesignation() {
        return designation;
    }

    public void setDesignation(Object designation) {
        this.designation = designation;
    }

    public Object getDcofficeName() {
        return dcofficeName;
    }

    public void setDcofficeName(Object dcofficeName) {
        this.dcofficeName = dcofficeName;
    }

    public Integer getDivisionId() {
        return divisionId;
    }

    public void setDivisionId(Integer divisionId) {
        this.divisionId = divisionId;
    }

    public Integer getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Integer districtId) {
        this.districtId = districtId;
    }

    public Integer getCityCorporationId() {
        return cityCorporationId;
    }

    public void setCityCorporationId(Integer cityCorporationId) {
        this.cityCorporationId = cityCorporationId;
    }

    public Integer getMunicipalId() {
        return municipalId;
    }

    public void setMunicipalId(Integer municipalId) {
        this.municipalId = municipalId;
    }

    public Integer getThanaId() {
        return thanaId;
    }

    public void setThanaId(Integer thanaId) {
        this.thanaId = thanaId;
    }

    public Integer getUnionId() {
        return unionId;
    }

    public void setUnionId(Integer unionId) {
        this.unionId = unionId;
    }

    public Object getWard() {
        return ward;
    }

    public void setWard(Object ward) {
        this.ward = ward;
    }

    public Object getArea() {
        return area;
    }

    public void setArea(Object area) {
        this.area = area;
    }

    public Object getEid() {
        return eid;
    }

    public void setEid(Object eid) {
        this.eid = eid;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Object getEmail() {
        return email;
    }

    public void setEmail(Object email) {
        this.email = email;
    }

    public Object getEmailVerifiedAt() {
        return emailVerifiedAt;
    }

    public void setEmailVerifiedAt(Object emailVerifiedAt) {
        this.emailVerifiedAt = emailVerifiedAt;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public Integer getSmsVerified() {
        return smsVerified;
    }

    public void setSmsVerified(Integer smsVerified) {
        this.smsVerified = smsVerified;
    }

    public Object getApiToken() {
        return apiToken;
    }

    public void setApiToken(Object apiToken) {
        this.apiToken = apiToken;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
